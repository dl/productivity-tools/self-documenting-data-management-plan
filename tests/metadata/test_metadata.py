import pytest
from unittest.mock import Mock

from sddmp.metadata.metadata import Metadata
from sddmp.metadata.metadata import dict_supplement
from sddmp.metadata.schema_org_validator import SchemaOrgValidator


@pytest.fixture
def mock_validator():
    # Create a mock SchemaOrgValidator instance
    mock_validator = Mock(spec=SchemaOrgValidator)

    # Mock the term_exists method to return True for specific terms
    def mock_term_exists(term):
        return term in ["Foo", "Bar", "a", "b"]

    mock_validator.term_exists.side_effect = mock_term_exists

    # Mock the get_term_details method to return a specific dict for specific terms
    def mock_get_term_details(term):
        # If the term doesn't exist, return a dict indicating that
        if not mock_validator.term_exists(term):
            return {"valid": False}
        return {
            "valid": True,
            "subject": f"http://schema.org/{term}",
            "predicate": "http://www.w3.org/2000/01/rdf-schema#label",
            "object": f"{term}",
        }

    mock_validator.get_term_details.side_effect = mock_get_term_details

    return mock_validator


@pytest.fixture
def metadata_instance(mock_validator):
    obj = Metadata({"Foo": {"a": 1, "b": 2}, "Bar": 42})
    obj.schema_org_validator = mock_validator
    return obj


def test_dict_supplement(metadata_instance):
    """
    Test the dictionary supplement function that underlies the Metadata.supplement method.
    """
    dict1 = dict(metadata_instance)
    other = {"Foo": {"b": 99, "c": 100}}
    expected = {"Foo": {"a": 1, "b": 2, "c": 100}, "Bar": 42}
    assert dict_supplement(dict1, other) == expected


def test_supplement(metadata_instance):
    """
    Test the Metadata.supplement method. This should work identically to the dict_supplement
    function, but it should also work in place.
    """
    metadata_instance.supplement(Metadata({"Foo": {"b": 99, "c": 100}}))
    expected = {"Foo": {"a": 1, "b": 2, "c": 100}, "Bar": 42}
    assert metadata_instance == expected


def test_all_keys(metadata_instance):
    """
    Test the Metadata.all_keys method.
    """
    assert set(metadata_instance.all_keys()) == set(["Foo", "Bar", "a", "b"])


@pytest.mark.parametrize(
    "metadata_dict, is_valid",
    [
        (
            {"Foo": {"a": 1, "b": 2}, "Bar": 42},
            True,
        ),
        (
            {"InvalidKey": {"a": 1, "b": 2}, "Bar": 42},
            False,
        ),
    ],
)
def test_is_valid(metadata_dict, is_valid, metadata_instance):
    """
    Test the Metadata.is_valid property.
    """
    metadata_instance.update(metadata_dict)
    assert metadata_instance.is_valid == is_valid


def test_validate_keys(metadata_instance):
    """
    Test that the Metadata.validate_keys method returns the correct results.
    """
    result = metadata_instance.validate_keys()

    # Check that the result is a dict
    assert isinstance(result, dict)

    # Check that the result contains the correct keys
    assert set(result.keys()) == set(["Foo", "Bar", "a", "b"])
