import pytest
from sddmp.filesystem import FileSystem
from sddmp.config import load_config


@pytest.fixture
def temp_dir(tmp_path):
    return tmp_path


@pytest.fixture
def directory(temp_dir):
    # Create a directory structure in temp_dir

    # temp_dir
    # ├── file1.txt
    # ├── subdir1
    # │   ├── file2.txt
    # │   ├── subsubdir1
    # │   │   └── file3.txt
    # │   └── subsubdir2
    # └── subdir2
    #     └── subsubdir1

    (temp_dir / "subdir1" / "subsubdir1").mkdir(parents=True)
    (temp_dir / "subdir1" / "subsubdir2").mkdir()
    (temp_dir / "subdir2" / "subsubdir1").mkdir(parents=True)
    (temp_dir / "file1.txt").write_text("Hello, world!")
    (temp_dir / "subdir1" / "file2.txt").write_text("Hello, again!")
    (temp_dir / "subdir1" / "subsubdir1" / "file3.txt").write_text("Hello, once more!")

    # Create a Directory instance for temp_dir
    fs = FileSystem(load_config())
    return fs.read_directory(temp_dir)


def test_name(directory):
    assert directory.name == directory.path.name


def test_path_depth(directory):
    assert directory.path_depth == 0

    for child in directory.children:
        assert child.path_depth == 1


def test_self_and_descendants(directory):
    assert len(directory.self_and_descendants) == 6
